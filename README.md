# Usage:  

Install

```
npm install musixmatch-node
```

Import

```javascript
const Musixmatch = require('musixmatch-node')
const mxm = new Musixmatch(/* YOUR API KEY */)
```

Get lyrics by track and artist

```javascript
const lyrics = await mxm.getLyricsMatcher({
  q_track: 'sick sick sick',
  q_artist: 'queens of the stone age',
})
```

